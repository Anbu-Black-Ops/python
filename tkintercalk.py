# -*-coding: utf-8-*-
# Requiere tkinter instalado:   
# fedora      dnf install python3-tkinter
# debian      apt-get install python3-tk

########################################################################
#                               IMPORT
########################################################################
from tkinter import *
from tkinter import ttk


########################################################################
#                   VARIABLES GLOBALES Y CONSTANTES
########################################################################


########################################################################
#                               NIVEL 2
########################################################################


########################################################################
#                               NIVEL 1 (MAIN)
########################################################################

#######################  FUNCIONES DESDE TKINTER  ######################
def sumar():
    operando1_temp = int(operando1_entry.get())
    operando2_temp = int(operando2_entry.get())
    
    resultado_temp= operando1_temp + operando2_temp
    
    resultado_label.config(text=str(resultado_temp))

def restar():
    operando1_temp = int(operando1_entry.get())
    operando2_temp = int(operando2_entry.get())
    
    resultado_temp= operando1_temp - operando2_temp
    
    resultado_label.config(text=str(resultado_temp))

def multiplicar():
    operando1_temp = int(operando1_entry.get())
    operando2_temp = int(operando2_entry.get())
    
    resultado_temp= operando1_temp * operando2_temp
    
    resultado_label.config(text=str(resultado_temp))


def dividir():
    operando1_temp = int(operando1_entry.get())
    operando2_temp = int(operando2_entry.get())
    
    resultado_temp= operando1_temp / operando2_temp
    
    resultado_label.config(text=str(resultado_temp))



###############################  MAIN  #################################

if __name__ == "__main__":
	# Definimos la ventana
	ventana = Tk()
	ventana.title("La mejor calculadora del mundo")
	ventana.geometry('400x400')

	# Definimos los elementos de la ventana
	operando1_label = ttk.Label(ventana, text="Operando 1:")
	operando1_entry= ttk.Entry(ventana, width=10)
	operando1_entry.insert(0, "0")


	operando2_label = ttk.Label(ventana, text="Operando 2:")
	operando2_entry= ttk.Entry(ventana, width=10)
	operando2_entry.insert(0, "0")

	boton_sumar = ttk.Button(ventana, text="+",
	                         command=lambda: sumar())
	boton_restar = ttk.Button(ventana, text="-",
	                         command=lambda: restar())

	boton_multiplicar = ttk.Button(ventana, text="*",
	                         command=lambda: multiplicar())

	boton_dividir = ttk.Button(ventana, text="/",
	                         command=lambda: dividir())

	resultado_titulo= ttk.Label(ventana, text="Resultado=")
	resultado_label = Label(ventana, text="0")
	 

	# Mostramos los elementos de la ventana
	operando1_label.pack()
	operando1_entry.pack()
	operando2_label.pack()
	operando2_entry.pack()
	boton_sumar.pack()
	boton_restar.pack()
	boton_multiplicar.pack()
	boton_dividir.pack()
	resultado_titulo.pack()
	resultado_label.pack()

	# Bucle de la ventana
	ventana.mainloop()
